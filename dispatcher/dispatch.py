"""
Evaluate requests received by Flask server.
"""

from dispatcher.cmdb import assets
from dispatcher.host import Host


def find_host(args, config):
    """
    Fetch systems from CMDB and search for a matching host by comparing MAC addresses.

    :param args: arguments submitted, from the HTTP request
    :param config: configuration object
    :return: matching CMDB host info; `None` if no match
    """
    hosts, status_code = assets(config.snipeit_url, config.snipeit_api_key)
    if status_code != 200:
        return hosts, status_code

    for host in hosts:
        if args["mac"] == host.mac:
            if host.os is None:
                host.os = config.default_os
            if host.release is None:
                host.release = config.default_release
            return host, status_code

    return None, 404


def ipxe(args, config):
    """
    Search the matching host and build and return a iPXE menu.

    :param args: arguments submitted, from the HTTP request
    :param config: configuration object
    :return:
    """
    host_found, status_code = find_host(args, config)
    if host_found is None:
        return "Host not found", 404
    if status_code == 401:
        return "Not authorized for CMDB", 500
    if status_code != 200:
        return "Unknown error", status_code

    return host_found.render_ipxe(config), status_code


def ks(args, config, hostname):
    """
    Build and return a kickstart file for this host.
    :param args:
    :param config:
    :param hostname:
    :return:
    """
    host_found, status_code = find_host(args, config)
    if host_found is None:
        return "Host not found", 404

    if isinstance(host_found, Host):
        host_found.ip = args["ip"]
        host_found.hostname = hostname
        host_found.netmask = args["netmask"]
        host_found.dns = args["dns"]
        host_found.gateway = args["gateway"]
        host_found.domain = args["domain"]

        return host_found.render_installfile(config), status_code
    else:
        return host_found, status_code
