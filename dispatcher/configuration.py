"""
Read configuration and environment variables.
"""

import json
import os
import logging


class PostBoot:
    """
    Semaphore/Ansible post boot configuration
    """

    server = ""
    token = ""  # nosec B105
    project = ""
    template = ""


class ConfigFile:
    """
    Configuration file wrapper. Evaluates configuration variables and overwrites
    settings, if available.

    Configuration is a JSON file. See config_example.json.
    """

    def __init__(self, filename):
        """
        Read configuration file and evaluate environment variables.

        :param filename:  file to open. A relative path will be evaluated
        starting from the current working directory.
        """
        self.server_url = ""
        self.snipeit_url = ""
        self.snipeit_api_key = ""
        self.ssh_keys = []
        self.default_os = "redhat"
        self.default_os_release = 8
        self.partition_schemes = {}
        self.available_os = []
        # Post installation configuration by Semaphore
        self.post_boot = PostBoot()

        conf_file = None
        try:
            conf_file = open("config.json")
        except FileNotFoundError:
            logging.error("Configuration file not found")
            exit(1)

        conf = None
        try:
            conf = json.load(conf_file)
        except json.JSONDecodeError as e:
            logging.error("Malformed configuration: ", e.msg)
            exit(1)

        # API key by env var only
        self.snipeit_api_key = os.getenv("SNIPEIT_API_KEY")
        if self.snipeit_api_key is None:
            logging.error("SnipeIT API key missing (env var SNIPEIT_API_KEY")
            exit(1)
        self.post_boot.token = os.getenv("SEMAPHORE_API_KEY")
        if self.post_boot.token is None:
            logging.warning("Semaphore API key missing")

        # possible override by env vars
        self.snipeit_url = os.getenv("SNIPEIT_URL", conf["snipeit_url"])
        self.default_os = os.getenv("DEFAULT_OS", conf["default_os"]["name"])
        self.default_os_release = os.getenv(
            "DEFAULT_OS_RELEASE", conf["default_os"]["release"]
        )
        self.server_url = os.getenv("SERVER_URL", conf["server_url"])

        # By configuration only
        try:
            self.partition_schemes = conf["partition_schemes"]
        except KeyError:
            logging.error("Partition mapping missing in configuration")
            exit(1)

        try:
            self.available_os = conf["available_os"]
        except KeyError:
            logging.error("List of available OS missing in configuration")
            exit(1)

        try:
            self.ssh_keys = conf["ssh_keys"]
        except KeyError:
            logging.warning("No SSH public keys configured")
            pass

        try:
            self.post_boot.server = conf["post_boot"]["server"]
            self.post_boot.project = conf["post_boot"]["project_id"]
            self.post_boot.template = conf["post_boot"]["template_id"]
        except KeyError:
            logging.warning("Post boot configuration missing or incomplete")
            pass

    def get_kickstart_uri(self, os_name, release):
        for item in self.available_os:
            if item["name"] == os_name and item["release"] == release:
                return item["kickstart_base_uri"], item["kickstart_appstream_uri"]

        logging.error("OS/release not found in available OS/release pairs")
        return None, None

    def get_partitioning(self, family, partitioning):
        return self.partition_schemes[family][partitioning]

    def get_repo_server(self, os_name, release):
        for item in self.available_os:
            if item["name"] == os_name and item["release"] == release:
                return item["repo_server"]
