{% block repos %}
url --url {{ kickstart_base_uri }}
repo --name Rocky_Linux_{{ host.release }}_for_x86_64_-_AppStream_Kickstart_{{ host.release }} --baseurl {{ kickstart_appstream_uri }}
{% endblock %}

lang en_US.UTF-8
selinux --enforcing
keyboard us
skipx

network --device={{ mac }} --hostname {{ host.hostname }}.{{ host.domain }} --noipv6 --bootproto static --ip={{ host.ip }} --netmask={{ host.netmask }} --gateway={{ host.gateway }} --mtu=1500 --nameserver={{ host.dns }}

rootpw --iscrypted $5$6mVDkPOCONHrj3LV$ipjLEPQe/V4854EEZj5ASPvj2QDETHu2Y54NbWh6zH8
firewall --service=ssh
authselect --useshadow --passalgo=sha256 --kickstart
{% if host.release == "8" %}
timezone --utc Europe/Zurich --ntpservers swisstime.ethz.ch
{% else %}
timezone --utc Europe/Zurich
timesource --ntp-server swisstime.ethz.ch
{% endif %}

services --disabled gpm,sendmail,cups,pcmcia,isdn,rawdevices,hpoj,bluetooth,openibd,avahi-daemon,avahi-dnsconfd,hidd,hplip,pcscd

bootloader --location=mbr --append="nofb quiet splash=quiet"

{% for item in partitioning -%}
{{ item }}
{% endfor %}

text
reboot
%packages
{% if host.release == "8" %}
python36
redhat-lsb-core
{% endif %}
perl
yum
dhclient
chrony
-ntp
wget
@Core
%end

%post --nochroot
exec < /dev/tty3 > /dev/tty3
#changing to VT 3 so that we can see whats going on....
/usr/bin/chvt 3
(
cp -va /etc/resolv.conf /mnt/sysimage/etc/resolv.conf
/usr/bin/chvt 1
) 2>&1 | tee /mnt/sysimage/root/install.postnochroot.log
%end

%post --log=/root/install.post.log
logger "Starting anaconda postinstall"
exec < /dev/tty3 > /dev/tty3
#changing to VT 3 so that we can see whats going on....
/usr/bin/chvt 3

# ens192 interface
real=`grep -l {{ host.mac }} /sys/class/net/*/{bonding_slave/perm_hwaddr,address} 2>/dev/null | awk -F '/' '// {print $5}' | head -1`
sanitized_real=`echo $real | sed s/:/_/`

cat << EOF > /etc/sysconfig/network-scripts/ifcfg-$sanitized_real
BOOTPROTO="none"
IPADDR="{{ host.ip }}"
NETMASK="{{ host.netmask }}"
GATEWAY="{{ host.gateway }}"
DOMAIN="ethz.ch"
DEVICE=$real
HWADDR="{{ host.mac }}"
ONBOOT=yes
PEERDNS=yes
PEERROUTES=yes
DEFROUTE=yes
DNS1="{{ host.dns }}"
MTU=1500
EOF

echo "Updating system time"
systemctl enable --now chronyd
/usr/bin/chronyc -a makestep
/usr/sbin/hwclock --systohc

{% if host.os == "redhat" %}
echo "Starting the subscription-manager registration process"

dnf -y install subscription-manager

curl -k https://id-sat-prd-02.ethz.ch/pub/katello-rhsm-consumer | /bin/bash
MAJOR=$(grep 'REDHAT_BUGZILLA_PRODUCT_VERSION' /etc/os-release | cut -f2 -d= | cut -f1 -d.)
subscription-manager register --org="COMMON" --activationkey="let-isg-${MAJOR}-server" --force
yum install -y katello-host-tools
{% endif %}

# update all the base packages from the updates repository
dnf -y update
dnf -y upgrade

user_exists=false
getent passwd root >/dev/null 2>&1 && user_exists=true
if $user_exists; then

  mkdir -p ~root/.ssh

  cat << EOF >> ~root/.ssh/authorized_keys
{%- for key in ssh_keys %}
{{ key }}
{%- endfor %}
EOF

  chmod 0700 ~root/.ssh
  chmod 0600 ~root/.ssh/authorized_keys
  chown -R root: ~root/.ssh

  # Restore SELinux context with restorecon, if it's available:
  command -v restorecon && restorecon -RvF ~root/.ssh || true

fi

# Post boot configuration trigger
cat << EOF > /usr/local/bin/first_boot.sh
curl -q -H 'Content-Type: application/json' -H "Authorization: Bearer {{post_boot.token}}" -d '{"template_id": {{post_boot.template}}, "limit": "{{host.hostname}}"}' https://{{post_boot.server}}/api/project/{{post_boot.project}}/tasks
systemctl disable first_boot
rm /usr/local/bin/first_boot.sh
EOF

cat << EOF >> /etc/systemd/system/first_boot.service
[Unit]
Description=First boot configuration service
After=sshd.service
[Install]
WantedBy=multi-user.target
[Service]
Type=oneshot
ExecStart=/usr/bin/bash /usr/local/bin/first_boot.sh
EOF

systemctl enable first_boot.service
%end

%end

%post --erroronfail

sync
%end
