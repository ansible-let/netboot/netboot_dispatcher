"""
Evaluate templates for HTTP responses.
"""

import jinja2

template_loader = jinja2.FileSystemLoader("dispatcher/templates")
env = jinja2.Environment(  # nosec
    loader=template_loader,
    autoescape=jinja2.select_autoescape(
        enabled_extensions=("html", "xml", "ipxe"),
        disabled_extensions=("ks", "cfg"),
        default=True,
    ),
)


class IpxeFile:
    """
    iPXE menu file.
    """

    def render(self, config, host):
        installer_uri = ""
        if host.family.lower() == "redhat":
            kickstart_base_uri, kickstart_appstream_uri = config.get_kickstart_uri(
                host.os, host.release
            )
            installer_uri = f"{kickstart_base_uri}images/pxeboot"

        if host.family.lower() == "debian":
            repo_server = config.get_repo_server(host.os, host.release)
            installer_uri = f"http://{repo_server}/debian/dists/{host.release_name}/main/installer-amd64/current/images/netboot/debian-installer/amd64/"
        template = env.get_template("index.ipxe")
        return template.render(
            {
                "host": host,
                "installer_uri": installer_uri,
                "server_url": config.server_url,
            }
        )


class UnattendedInstallationFile:
    """
    General class for OS specific installer files.
    """

    def render(self, config, host):
        return NotImplementedError


class PreseedFile(UnattendedInstallationFile):
    def render(self, config, host):
        repo_server = config.get_repo_server(host.os, host.release)
        if not repo_server:
            return "Repo server not found"
        try:
            partitioning = config.get_partitioning(host.family, host.partitioning)
        except KeyError:
            return "Partition scheme not found"

        template_file = f"{host.os}_preseed.cfg"
        template = env.get_template(template_file)
        return template.render(
            {
                "host": host,
                "repo_server": repo_server,
                "ssh_keys": config.ssh_keys,
                "partitioning": partitioning,
                "post_boot": config.post_boot,
            }
        )


class KickstartFile(UnattendedInstallationFile):
    """
    Kickstart file. Templates are named after

    *<os>*_kickstart.ks

    Partitioning instructions (<block partitioning> in the template) are read from configuration
    and selected according to the CMDB, where the names of the partioning schemes are used as option.
    """

    def render(self, config, host):
        kickstart_base_uri, kickstart_appstream_uri = config.get_kickstart_uri(
            host.os, host.release
        )
        if not kickstart_base_uri or not kickstart_appstream_uri:
            return "Repo URI not found"
        partitioning = config.get_partitioning(host.family, host.partitioning)
        if not partitioning:
            return "Partition scheme not found"

        # Kickstart file based on OS
        template_file = f"{host.os}_kickstart.ks"
        template = env.get_template(template_file)
        return template.render(
            {
                "host": host,
                "kickstart_base_uri": kickstart_base_uri,
                "kickstart_appstream_uri": kickstart_appstream_uri,
                "ssh_keys": config.ssh_keys,
                "partitioning": config.get_partitioning(host.family, host.partitioning),
                "post_boot": config.post_boot,
            }
        )
