FROM python:3.10
ARG VCS_TAG

LABEL maintainer="Bengt Giger <bgiger@ethz.ch>"
ENV PYTHONUNBUFFERED=1

COPY app.py requirements.txt /app/
COPY dispatcher /app/dispatcher

WORKDIR /app
RUN pip install --proxy=http://proxy.ethz.ch:3128 -r requirements.txt
RUN echo $VCS_TAG >.version.txt

RUN groupadd --system py && useradd --system --gid py py
USER py:py

ENV FLASK_CONFIG=production
CMD gunicorn -b 0.0.0.0:5000 -t 120 -w 4 app:app
